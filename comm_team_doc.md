# Community Team write-up

**Mission:** making the forum an enjoyable experience for its members

**Methods:** 

- moderate the forum
- help out users
- suggest forum changes to the rest of the staff
- suggest rule changes to the rest of the staff
- be a good example
- keep the peace

**Other tasks:**

- manage Discord
- moderate Twitch chat
- be active in staff-discussions

**Management:** 

The main communication medium is the Community Team Discord used for temporary discussions. On the forum there is a moderation history-board where there is a thread for each non-trivially moderated member to act as a ledger that includes the explanation and evidence for the actions taken. This allows for easy access to the relevant information about the users (and their past crimes) and for transparency within the staff. There is one team leader who is active in team leader-discussions and makes sure the Community Team maintains its vision.

### Guidelines for all staff-members:

- We are always looking to get new people to join the community, and the staff-members are the ambassadors of the community. Be careful with behavior that is likely to discourage people to join the community.

- Likewise, we are always looking to retain the people in our community. Refrain from making the members of the community feel unwelcome; keep your toxicity to a minimum.

## General moderation policy

**Our mission is not to enforce the forum guidelines.** We are here to make the community enjoyable to as many as possible while remaining in line with the core mission (explained elsewhere). The forum guidelines can be something to fall back on if needed; often the guidelines can be "broken" without a problem. Examples of this follow:

- Player A asks about a strategy for France vs India. Player B suggests that using Germany against India is better. Others go into discussion about whether Player B is correct or not. While this is technically off-topic, it can still be a useful discussion and helpful to the OP.

- A new Korean player with mediocre English joins the forum and another player talks to him in Korean.

- Jokes. Sometimes impersonating members, making triple-posts, insulting others, or going blatantly off-topic is fine because it is funny.

### Direct action

Against gross spam, cheating accusations, inappropriate content, distributions of cheats, doxing, advertisements: take direct action. You can act as soon as you see this happening either by deleting the post, editing it, or temporarily moving it to the staff boards if you are unsure or do not have the opportunity to take proper action at the moment. 

### Other actions

Against minor violations such as light spam or insults a public warning is usually sufficient. This means that you reference the post and explain why it is bad; do not quote the forum guidelines. If the user does not stop, further action can be taken.

- Example A: Mitoe insults _H2O by saying he is a dictator. No action is needed because this is clearly a mockery of everyone who does not realize this truth and thus a joke.

- Example B: Jerom insults Diarouga by saying he is a dangerous crackhead and Diarouga subsequently reports the post as offensive. A Community Team-member publicly tells Jerom to stop insulting.

- Example C: Jerom insults Diarouga once again despite the public warning. This time the Community Team-member can edit/delete the post and give a private warning.

### Flame wars

When a flame war is going on and a Community Team-member steps in, it is best to not punish either side. One of them started the flame war, but the other should have ignored or reported it, not started insulting as well. Because of this, it’s best to publicly tell them to stop and make sure to say that every off-topic posts from that point on will be removed.

### Going off-topic

When a thread is going very off-topic (either not a useful discussion at all, or hindering the original discussion) it is also best to not delete posts until a public warning has shown to not be sufficient. Make sure you only act when somebody is annoyed by the off-topic discussion; don ot just because you feel like it. Going off-topic isn’t inherently bad.

